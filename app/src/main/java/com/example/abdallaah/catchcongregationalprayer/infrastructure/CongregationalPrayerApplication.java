package com.example.abdallaah.catchcongregationalprayer.infrastructure;

import android.app.Application;
import android.util.Log;

import com.example.abdallaah.catchcongregationalprayer.live.Module;
import com.squareup.otto.Bus;

public class CongregationalPrayerApplication extends Application {
    private static final String TAG = CongregationalPrayerApplication.class.getName();

    public static final String API_KEY = "AIzaSyDa8pmi7e1fJn0ia0eSq8lKkAw1QVOf_Ss";
    public static final String GOOGLE_MAPS_API_URL =
            "https://maps.googleapis.com/maps/api/place/nearbysearch/";

    protected Bus bus;

    public CongregationalPrayerApplication() {
        bus = new Bus();
    }

    public Bus getBus() {
        Log.d(TAG, "getBus: starts");
        Log.d(TAG, "getBus() returned: " + bus);
        Log.d(TAG, "getBus: ends");
        return bus;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate: starts");
        super.onCreate();
        Module.Register(this);
        Log.d(TAG, "onCreate: ends");
    }
}
