package com.example.abdallaah.catchcongregationalprayer.models;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MosqueJSONList {
    @SerializedName("results") public List<MosqueJSON> mosqueJSONList;

}
