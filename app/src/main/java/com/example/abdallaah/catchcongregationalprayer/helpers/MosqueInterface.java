package com.example.abdallaah.catchcongregationalprayer.helpers;

import com.example.abdallaah.catchcongregationalprayer.models.MosqueJSONList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MosqueInterface {
    @GET("json")
    Call<MosqueJSONList> loadMosques(
            @Query("location") String location,
            @Query("radius") String radius,
            @Query("types") String type,
            @Query("sensor") String sensor,
            @Query("key") String API_KEY
    );
}




//https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=40.7038,-73.7805&radius=5000&types=mosque&sensor=true&key=AIzaSyBPiLr2zfW6HWax8NqDHJ9WTaR0v0tIJvw
//https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=40.7038,-73.7805&radius=5000&types=mosque&sensor=true&key=AIzaSyBPiLr2zfW6HWax8NqDHJ9WTaR0v0tIJvw
//https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=40.7038,-73.7805&radius=5000&types=mosuqe&sensor=true&key=AIzaSyDa8pmi7e1fJn0ia0eSq8lKkAw1QVOf_Ss
//https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-73.7805,-73.7805&radius=5000&types=mosque&sensor=true&key=AIzaSyDa8pmi7e1fJn0ia0eSq8lKkAw1QVOf_Ss