package com.example.abdallaah.catchcongregationalprayer.models.nested_models;

import com.google.gson.annotations.SerializedName;

public class MosqueLatLong {

    @SerializedName("lat") Double latitude;
    @SerializedName("lng") Double longitude;



    public Double getLatitude() { return latitude; }

    public Double getLongitude() {
        return longitude;
    }
}
