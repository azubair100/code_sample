package com.example.abdallaah.catchcongregationalprayer.models.nested_models;


import com.google.gson.annotations.SerializedName;


public class MosqueGeometry {
    @SerializedName("location") public MosqueLocation mosqueLocations;

    public MosqueGeometry() {
    }

    public MosqueLocation getMosqueLocations() {
        return mosqueLocations;
    }
}
