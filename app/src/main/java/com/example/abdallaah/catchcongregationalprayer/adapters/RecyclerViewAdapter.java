package com.example.abdallaah.catchcongregationalprayer.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.abdallaah.catchcongregationalprayer.R;
import com.example.abdallaah.catchcongregationalprayer.models.Mosque;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MosqueHolder>{

    private List<Mosque> mosques;
    private Context mContext;

    public RecyclerViewAdapter(List<Mosque> mosques, Context mContext) {
        this.mosques = mosques;
        this.mContext = mContext;
    }

    @Override
    public RecyclerViewAdapter.MosqueHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mosque_row, parent, false);
        return new MosqueHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.MosqueHolder holder, int position) {
        if (mosques == null || mosques.size() == 0) {
            holder.mosqueName.setText("No Mosque Found");
        } else {
            Mosque mosque = mosques.get(position);
            holder.mosqueName.setText(mosque.getMosqueName());
            holder.mosqueAddress.setText(mosque.getMosqueName());
            holder.mosqueDistance.setText(mosque.getMosqueName());


        }
    }

    @Override
    public int getItemCount() {

        return ((mosques != null) && (mosques.size() > 0) ? mosques.size() : 1);
    }


    static class MosqueHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name) TextView mosqueName;
        @BindView(R.id.address) TextView mosqueAddress;
        @BindView(R.id.milesAway) TextView mosqueDistance;




        public MosqueHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
