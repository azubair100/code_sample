package com.example.abdallaah.catchcongregationalprayer.services;


import android.util.Log;

import com.example.abdallaah.catchcongregationalprayer.models.Mosque;

import java.util.ArrayList;

public class MosqueServices {
    private static final String TAG = MosqueServices.class.getName();

    public MosqueServices() { Log.d(TAG, "Constructor called: "); }

    public static class SearchMosqueRequest{
        private static final String TAG = "SearchMoviesRequest";
        public String latitude;
        public String longitude;
        public String radius;

        public SearchMosqueRequest(String latitude, String longitude, String radius) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.radius = radius;
        }
    }

    public static class SearchMosqueResponse{
        public ArrayList<Mosque> mosques;

    }
}
