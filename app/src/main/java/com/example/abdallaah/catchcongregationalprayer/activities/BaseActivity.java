package com.example.abdallaah.catchcongregationalprayer.activities;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.abdallaah.catchcongregationalprayer.infrastructure.CongregationalPrayerApplication;
import com.squareup.otto.Bus;

public class BaseActivity extends AppCompatActivity{
    private static final String TAG = BaseActivity.class.getName();
    protected CongregationalPrayerApplication application;
    protected Bus bus;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: starts");
        super.onCreate(savedInstanceState);
        application = (CongregationalPrayerApplication) getApplication();
        bus = application.getBus();
        bus.register(this);
        Log.d(TAG, "onCreate: ends");
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: starts");
        super.onDestroy();
        bus.unregister(this);
        Log.d(TAG, "onDestroy: ends");
    }


}
