package com.example.abdallaah.catchcongregationalprayer.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abdallaah.catchcongregationalprayer.R;
import com.example.abdallaah.catchcongregationalprayer.models.Mosque;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;


public class MapsFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = MapsFragment.class.getName();


    private ArrayList<Mosque> mosques;
    private String latitude;
    private String longitude;
    private GoogleMap mMap;
    private Fragment newFragment;


    public MapsFragment(ArrayList<Mosque> mosques, String latitude, String longitude) {
        Log.d(TAG, "MapsFragment: CONSTRUCTOR starts");
        this.mosques = mosques;
        this.latitude = latitude;
        this.longitude = longitude;

        Log.d(TAG, "MapsFragment: CONSTRUCTOR ends");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maps, container, false);

        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        SupportMapFragment fragment = new SupportMapFragment();
        transaction.add(R.id.mosque_map, fragment);
        transaction.commit();

        fragment.getMapAsync(this);


        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.clear();

        LatLng userLocation = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        mMap.addMarker(new MarkerOptions().position(userLocation).title("You are here"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLocation, 12));

        placesMosqueMarkers();
        setUpMarkerClick();
        setUpMapClick();
    }

    private void placesMosqueMarkers() {

        if (mosques != null && mosques.size() > 0) {
            for (Mosque mosque : mosques) {
                Marker marker =
                mMap.addMarker(
                        new MarkerOptions()
                                .position(
                                        new LatLng(
                                                Double.parseDouble(mosque.getLatitude()),
                                                Double.parseDouble(mosque.getLongitude())
                                        )
                                )
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                );
                marker.setTag(mosque);
                marker.hideInfoWindow();
            }
        }

    }

    private void setUpMarkerClick(){

        mMap.setOnMarkerClickListener(
                new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        removeMosqueInfoFragment();
                        newFragment = new MapRowFragment((Mosque) marker.getTag());
                        getActivity().
                                getSupportFragmentManager().
                                beginTransaction().
                                add(
                                        R.id.maps_fragment_frame_layout,
                                        newFragment
                                ).
                                addToBackStack(null).
                                commit();
                        return false;
                    }
                }
        );

    }

    private void setUpMapClick(){
        mMap.setOnMapClickListener(
                new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng arg0) {
                        removeMosqueInfoFragment();
                    }
                });
    }

    private void removeMosqueInfoFragment(){
        if(newFragment != null) {
            getActivity().
                    getSupportFragmentManager().
                    beginTransaction().
                    remove(newFragment).
                    addToBackStack(null).
                    commit();
        }
    }


}
