package com.example.abdallaah.catchcongregationalprayer.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.example.abdallaah.catchcongregationalprayer.R;
import com.example.abdallaah.catchcongregationalprayer.fragments.DefaultMosqueFragment;
import com.example.abdallaah.catchcongregationalprayer.fragments.MapsFragment;
import com.example.abdallaah.catchcongregationalprayer.fragments.MosqueListFragment;
import com.example.abdallaah.catchcongregationalprayer.fragments.SearchFragment;
import com.example.abdallaah.catchcongregationalprayer.fragments.SettingsFragment;
import com.example.abdallaah.catchcongregationalprayer.helpers.BottomNavigationViewHelper;
import com.example.abdallaah.catchcongregationalprayer.models.Mosque;
import com.example.abdallaah.catchcongregationalprayer.services.MosqueServices;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class LandingPageActivity extends BaseActivity implements
        GoogleApiClient.OnConnectionFailedListener{

    private static final String TAG = LandingPageActivity.class.getName();

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    LocationManager locationManager;
    LocationListener locationListener;

    private GoogleApiClient mGoogleApiClient;
    private  int currentMenu  = 0;

    public static ArrayList<Mosque> mosques;
    public static String latitude;
    public static String longitude;


    @BindView(R.id.landing_page_bottom_navigation) BottomNavigationView navigation;
    @BindView(R.id.landing_page_main_constraint) ConstraintLayout landingPageConstraint;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: started");
        super.onCreate(savedInstanceState);
        setupBottomNavigationListener();
        setContentView(R.layout.activity_landing_page);
        ButterKnife.bind(this);
        this.setupViews();
        initializeLocationVariables();
        mosques = new ArrayList<>();
        Log.d(TAG, "onCreate: ended");
    }

    private void setupViews(){
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (LeakCanary.isInAnalyzerProcess(this)) {return;}
        LeakCanary.install(getApplication());

        Logger.addLogAdapter(new AndroidLogAdapter());

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

    }

    private void setupBottomNavigationListener(){
        Log.d(TAG, "setupBottomNavigationListener: starts");
        mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Log.d(TAG, String.valueOf(item.getItemId()));
                if (currentMenu == item.getItemId()) {return false;}
                currentMenu = item.getItemId();
                switch (item.getItemId()) {
                    case R.id.default_mosque:
                        DefaultMosqueFragment defaultMosqueFragment = DefaultMosqueFragment.newInstance(null, null);
                        return replaceCurrentFragmentWith(defaultMosqueFragment);
                    case R.id.mosques_list:
                        MosqueListFragment listFragment = MosqueListFragment.newInstance();
                        return replaceCurrentFragmentWith(listFragment);
                    case R.id.mosques_map:
                        MapsFragment mapsFragment = new MapsFragment(null, null, null);
                        return replaceCurrentFragmentWith(mapsFragment);
                    case R.id.search_mosque:
                        SearchFragment searchFragment = SearchFragment.newInstance(null, null);
                        return replaceCurrentFragmentWith(searchFragment);
                    case R.id.settings:
                        SettingsFragment settingsFragment = SettingsFragment.newInstance(null, null);
                        return replaceCurrentFragmentWith(settingsFragment);
                }
                return false ;
            }

        };
        Log.d(TAG, "setupBottomNavigationListener: ends");
    }

    private boolean replaceCurrentFragmentWith(Fragment newFragment) {
        Log.d(TAG, "replaceCurrentFragmentWith: starts");
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        //animation here
        transaction.replace(R.id.landing_page_fragment_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
        Log.d(TAG, "replaceCurrentFragmentWith: ends");
        return true ;
    }

    @Subscribe
    public void getMosqueMessage(MosqueServices.SearchMosqueResponse response){
        Log.d(TAG, "getMovieMessage: starts");
        mosques.clear();
        mosques.addAll(response.mosques);
        Log.d(TAG, "getMovieMessage: ends");
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void initializeLocationVariables(){
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

    }

    @Override
    public void onResume() {
        super.onResume();
        if (checkLocationPermission()) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission. ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                //Request location updates:
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 400, 1, locationListener);
                Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if(lastKnownLocation != null){
                    latitude = lastKnownLocation.getLatitude() + "";
                    longitude = lastKnownLocation.getLongitude() + "";
                    bus.post(
                            new MosqueServices.SearchMosqueRequest(
                                    latitude, longitude, "5000"
                            ));

                }

            }
        }
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission. ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission. ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Need Location Services")
                        .setMessage("This app requires your location services to display nearby mosques")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getParent(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission. ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission. ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 400, 1, locationListener);

//                        }
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

        }
    }



}
//todo set up firebase
//todo set up firebase saving procedure
//todo on click view more information
//todo google maps api client
//todo get the pictures
//todo camera api


