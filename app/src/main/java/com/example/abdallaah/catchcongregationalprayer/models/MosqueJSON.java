package com.example.abdallaah.catchcongregationalprayer.models;


import com.example.abdallaah.catchcongregationalprayer.models.nested_models.MosqueGeometry;
import com.google.gson.annotations.SerializedName;

public class MosqueJSON {
    @SerializedName("place_id") String placeId;
    @SerializedName("name") String mosqueName;
    @SerializedName("vicinity") String mosqueVicinity;
    @SerializedName("geometry") MosqueGeometry mosqueGeometry;


    public MosqueJSON() {
    }

    public String getPlaceId() {
        return placeId;
    }

    public String getMosqueName() {
        return mosqueName;
    }

    public String getMosqueVicinity() {
        return mosqueVicinity;
    }

    public MosqueGeometry getMosqueGeometry() {
        return mosqueGeometry;
    }
}
