package com.example.abdallaah.catchcongregationalprayer.fragments;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.example.abdallaah.catchcongregationalprayer.Listeners.RecyclerItemClickListener;
import com.example.abdallaah.catchcongregationalprayer.R;
import com.example.abdallaah.catchcongregationalprayer.activities.LandingPageActivity;
import com.example.abdallaah.catchcongregationalprayer.adapters.RecyclerViewAdapter;
import com.example.abdallaah.catchcongregationalprayer.helpers.MosqueInterface;
import com.example.abdallaah.catchcongregationalprayer.models.Mosque;
import com.example.abdallaah.catchcongregationalprayer.models.MosqueJSON;
import com.example.abdallaah.catchcongregationalprayer.models.MosqueJSONList;
import com.example.abdallaah.catchcongregationalprayer.services.MosqueServices;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MosqueListFragment extends Fragment implements
        RecyclerItemClickListener.OnRecyclerClickListener{
    private static final String TAG = MosqueListFragment.class.getName();

    public ArrayList<Mosque> mosques;
    private RecyclerViewAdapter recyclerViewAdapter;


    @BindView(R.id.mosque_list_fragment_constraint) ConstraintLayout mainConstraint;
    @BindView(R.id.mosque_list_fragment_recycler_view) RecyclerView recyclerView;
    @BindView(R.id.mosque_list_fragment_constraint_progress_bar) ProgressBar progressBar;
    @BindView(R.id.mosque_list_fragment_toolbar_button) ImageButton button;
    @BindView(R.id.mosque_list_fragment_appbar) AppBarLayout appBarLayout;


    public MosqueListFragment() {
        // Required empty public constructor
    }

    public static MosqueListFragment newInstance() {
        MosqueListFragment fragment = new MosqueListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: starts");
        super.onCreate(savedInstanceState);
        mosques = LandingPageActivity.mosques;
        Log.d(TAG, "onCreate: ends");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "onCreateView: starts");
        View view = inflater.inflate(R.layout.fragment_mosque_list, container, false);
        ButterKnife.bind(this, view);
        this.setUpViews();
        Log.d(TAG, "onCreateView: ends");
        return view;
    }

    private void setUpViews(){
        Log.d(TAG, "setUpViews: starts");
        showOnMap();

        appBarLayout.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        //starting gesture detector
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), recyclerView, this)
        );

        recyclerViewAdapter = new RecyclerViewAdapter(mosques, getContext());
        recyclerView.setAdapter(recyclerViewAdapter);
        progressBar.setVisibility(View.GONE);
        appBarLayout.setVisibility(View.VISIBLE);

        Log.d(TAG, "setUpViews: ends");

    }
    
    
    private void showOnMap(){
        Log.d(TAG, "showOnMap: starts");
        button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, "onClick: clicked");

                        getActivity().
                                getSupportFragmentManager().
                                beginTransaction().
                                replace(
                                        R.id.landing_page_fragment_container,
                                        new MapsFragment(
                                                mosques,
                                                LandingPageActivity.latitude,
                                                LandingPageActivity.longitude
                                        )
                                ).
                                addToBackStack(null).
                                commit();
                    }
                }
        );
        Log.d(TAG, "showOnMap: ends");
    }


    @Subscribe
    public void getMosqueMessage(MosqueServices.SearchMosqueResponse response){
        Log.d(TAG, "getMovieMessage: starts");
        mosques.clear();
        mosques.addAll(response.mosques);

        Log.d(TAG, "getMovieMessage: ends");
    }


    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public void onItemLongClick(View view, int position) {

    }
}
