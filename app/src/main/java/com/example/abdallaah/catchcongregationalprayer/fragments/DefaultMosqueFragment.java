package com.example.abdallaah.catchcongregationalprayer.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abdallaah.catchcongregationalprayer.R;
import com.example.abdallaah.catchcongregationalprayer.activities.LandingPageActivity;

public class DefaultMosqueFragment extends Fragment {
    private static final String TAG = DefaultMosqueFragment.class.getName();


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;


    public DefaultMosqueFragment() {
        // Required empty public constructor
    }

    public static DefaultMosqueFragment newInstance(String param1, String param2) {
        DefaultMosqueFragment fragment = new DefaultMosqueFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_default_mosque, container, false);
    }

}
