package com.example.abdallaah.catchcongregationalprayer.models.nested_models;

import com.google.gson.annotations.SerializedName;



public class MosqueLocation {

    @SerializedName("lat") String latitude;
    @SerializedName("lng") String longitude;



    public String getLatitude() { return latitude; }

    public String getLongitude() {
        return longitude;
    }
}
