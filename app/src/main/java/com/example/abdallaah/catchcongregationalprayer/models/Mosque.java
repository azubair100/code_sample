package com.example.abdallaah.catchcongregationalprayer.models;

public class Mosque {
    private String placesId;
    private String mosqueName;
    private String mosqueVicinity;
    private String latitude;
    private String longitude;

    public Mosque(
            String placesId,
            String mosqueName,
            String mosqueVicinity,
            String latitude,
            String longitude) {
        this.placesId = placesId;
        this.mosqueName = mosqueName;
        this.mosqueVicinity = mosqueVicinity;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getPlacesId() {
        return placesId;
    }

    public String getMosqueName() {
        return mosqueName;
    }

    public String getMosqueVicinity() {
        return mosqueVicinity;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

}
