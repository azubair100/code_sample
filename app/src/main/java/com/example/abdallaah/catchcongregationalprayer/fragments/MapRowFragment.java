package com.example.abdallaah.catchcongregationalprayer.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.abdallaah.catchcongregationalprayer.R;
import com.example.abdallaah.catchcongregationalprayer.models.Mosque;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapRowFragment extends Fragment implements View.OnClickListener{
    private static final String TAG = MapRowFragment.class.getName();
    @BindView(R.id.name) TextView mosqueName;
    @BindView(R.id.address) TextView mosqueAddress;
    @BindView(R.id.milesAway) TextView mosqueMilesAway;

    private Mosque mosque;

    public MapRowFragment(Mosque mosque) {
        Log.d(TAG, "MapRowFragment: starts");
        this.mosque = mosque;
        Log.d(TAG, "MapRowFragment: ends"); 
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "onCreateView: starts");
        View view = inflater.inflate(R.layout.fragment_map_row, container, false);
        ButterKnife.bind(this, view);
        setMosqueInformation();
        Log.d(TAG, "onCreateView: ends");
        view.setOnClickListener(this);
        return view;
    }

    private void setMosqueInformation(){
        Log.d(TAG, "setMosqueInformation: starts");
        if(mosque != null) {
            mosqueName.setText(mosque.getMosqueName());
            mosqueAddress.setText(mosque.getMosqueVicinity());
            mosqueMilesAway.setText("");
        }
        Log.d(TAG, "setMosqueInformation: ends");
    }

    @Override
    public void onClick(View view) {
        Log.d(TAG, "onClick: clicked");
    }
}
