package com.example.abdallaah.catchcongregationalprayer.live;


import com.example.abdallaah.catchcongregationalprayer.models.MosqueJSONList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MosqueWebServices {

    @GET("json")
    Call<MosqueJSONList> loadMosques(
            @Query("location") String location,
            @Query("radius") String radius,
            @Query("types") String type,
            @Query("sensor") String sensor,
            @Query("key") String API_KEY
    );
}
