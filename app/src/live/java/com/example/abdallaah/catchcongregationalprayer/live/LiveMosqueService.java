package com.example.abdallaah.catchcongregationalprayer.live;

import android.util.Log;
import android.view.View;

import com.example.abdallaah.catchcongregationalprayer.adapters.RecyclerViewAdapter;
import com.example.abdallaah.catchcongregationalprayer.infrastructure.CongregationalPrayerApplication;
import com.example.abdallaah.catchcongregationalprayer.models.Mosque;
import com.example.abdallaah.catchcongregationalprayer.models.MosqueJSON;
import com.example.abdallaah.catchcongregationalprayer.models.MosqueJSONList;
import com.example.abdallaah.catchcongregationalprayer.services.MosqueServices;
import com.orhanobut.logger.Logger;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LiveMosqueService extends BaseLiveService{
    private static final String TAG = LiveMosqueService.class.getName();

    public LiveMosqueService(CongregationalPrayerApplication application, MosqueWebServices api) {
        super(application, api);
        Log.d(TAG, "CONSTRUCTOR: starts");
        Log.d(TAG, "CONSTRUCTOR: ends");
    }

    @Subscribe
    public void getMosqueMessage(MosqueServices.SearchMosqueRequest request){
        Log.d(TAG, "getMosqueMessage: starts");
        final MosqueServices.SearchMosqueResponse movieResponse = new MosqueServices.SearchMosqueResponse();
        movieResponse.mosques = new ArrayList<>();

        //parameter
        Call<MosqueJSONList> call =
                api.loadMosques(
                        request.latitude + "," + request.longitude,
                        request.radius,
                        "mosque",
                        "true",
                        CongregationalPrayerApplication.API_KEY);
        Log.d(TAG, "getMosqueMessage: call " + call);
        call.enqueue(
                new Callback<MosqueJSONList>() {
                    @Override
                    public void onResponse(Call<MosqueJSONList> call,
                                           Response<MosqueJSONList> response) {
                        Log.d(TAG, "onResponse: enqueued the call " + response.body().mosqueJSONList);
                        if(response.body() != null) {
                            movieResponse.mosques.clear();
                            for (MosqueJSON mosqueJSON : response.body().mosqueJSONList) {

                                Mosque mosque = new Mosque(
                                        mosqueJSON.getPlaceId(),
                                        mosqueJSON.getMosqueName(),
                                        mosqueJSON.getMosqueVicinity(),
                                        mosqueJSON.getMosqueGeometry().getMosqueLocations().getLatitude(),
                                        mosqueJSON.getMosqueGeometry().getMosqueLocations().getLongitude()
                                );
                                movieResponse.mosques.add(mosque);
                            }

                            bus.post(movieResponse);

                        }
                        Logger.i(movieResponse.mosques.toString());

                        Log.d(TAG, "onResponse: enqueued call ends");
                    }

                    @Override
                    public void onFailure(Call<MosqueJSONList> call, Throwable t) {

                    }
                });


        Log.d(TAG, "getMosqueMessage: ends");
    }
}
