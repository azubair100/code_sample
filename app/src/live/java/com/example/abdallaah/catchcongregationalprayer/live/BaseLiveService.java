package com.example.abdallaah.catchcongregationalprayer.live;

import android.util.Log;

import com.example.abdallaah.catchcongregationalprayer.infrastructure.CongregationalPrayerApplication;
import com.squareup.otto.Bus;

public class BaseLiveService {
    private static final String TAG = BaseLiveService.class.getName();

    protected CongregationalPrayerApplication application;
    protected Bus bus;
    protected MosqueWebServices api;

    public BaseLiveService(CongregationalPrayerApplication application, MosqueWebServices api) {
        Log.d(TAG, "CONSTRUCTOR: starts");
        this.application = application;
        this.api = api;
        bus = application.getBus();
        bus.register(this);
        Log.d(TAG, "CONSTRUCTOR: ends");
    }
}
