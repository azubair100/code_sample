package com.example.abdallaah.catchcongregationalprayer.live;

import android.util.Log;

import com.example.abdallaah.catchcongregationalprayer.infrastructure.CongregationalPrayerApplication;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Module{
    private static final String TAG = Module.class.getName();


    public static void Register(CongregationalPrayerApplication application){
        Log.d(TAG, "Register: starts");
        new LiveMosqueService(application, createMosqueService());
        Log.d(TAG, "Register: ends");
    }

    public static MosqueWebServices createMosqueService(){
        Log.d(TAG, "createMosqueService: starts");

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().
                baseUrl(CongregationalPrayerApplication.GOOGLE_MAPS_API_URL).
                client(client).
                addConverterFactory(GsonConverterFactory.create()).
                build();

        return retrofit.create(MosqueWebServices.class);

    }
}
